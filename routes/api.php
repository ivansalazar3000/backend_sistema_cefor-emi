<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\UnidadAcademicaController;
use App\Http\Controllers\PersonaController;
use App\Http\Controllers\TipoReporteController;


use App\Http\Controllers\TipoCompaniaController;
use App\Http\Controllers\TipoDocumentoController;
use App\Http\Controllers\TipoAsistenciaController;
use App\Http\Controllers\EscuadraController;
use App\Http\Controllers\SeccionController;
use App\Http\Controllers\CompaniaController;
use App\Http\Controllers\EstadoAsistenciaController;
use App\Http\Controllers\EstadoPersonaController;
use App\Http\Controllers\EstadoDocumentoController;


use App\Http\Controllers\CompaniaSeccionController;
use App\Http\Controllers\CompaniaSeccionEscuadraController;
use App\Http\Controllers\CompSecEscPersonaController;
use App\Http\Controllers\FiliacionController;
use App\Http\Controllers\DocumentacionController;
use App\Http\Controllers\AsistenciaController;
use App\Http\Controllers\CertificadoController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('uploadFile', function (Request $request) {
    try {
        if ($request->hasFile('File')) {
            $fileName = md5(uniqid() . \Carbon\Carbon::now()) . '.' . strtolower($request->file('File')->getClientOriginalExtension());
            //dd($request->op);
            $path = $request->file('File')->storeAs('documents', $fileName, 'public');

            $data = array(
                'success' => true,
                'data' => $fileName,
                'a'  => $path,
                'msg' => trans('messages.file_uplodaded')
            );
        } else {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => 'Error al guardar archivo.'
            );
        }
    } catch (\Exception $e) {
        $data = array(
            'success' => false,
            'data' => null,
            'msg' => $e->getMessage()
        );
    }
    return response()->json($data);
})->name('utils.uploadFile');

Route::post('login', [LoginController::class, 'login'])->middleware('guest');
Route::post('loginToken365', [LoginController::class, 'loginToken365'])->middleware('guest');

/****************************************** ROL *********************************/
Route::group(['prefix' => 'Rol', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [RolController::class, 'list'])->name('Rol.list');
    Route::get('/index', [RolController::class, 'index'])->name('Rol.index');
    Route::post('/destroy', [RolController::class, 'destroy'])->name('Rol.destroy');
    Route::post('/store', [RolController::class, 'store'])->name('Rol.store');
    Route::get('/show', [RolController::class, 'show'])->name('Rol.show');
});

/************************************ UNIDAD ACADEMICA *****************/
Route::group(['prefix' => 'UnidadAcademica', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [UnidadAcademicaController::class, 'list'])->name('UnidadAcademica.list');
    Route::get('/index', [UnidadAcademicaController::class, 'index'])->name('UnidadAcademica.index');
    Route::post('/destroy', [UnidadAcademicaController::class, 'destroy'])->name('UnidadAcademica.destroy');
    Route::post('/store', [UnidadAcademicaController::class, 'store'])->name('UnidadAcademica.store');
    Route::get('/show', [UnidadAcademicaController::class, 'show'])->name('UnidadAcademica.show');
});

/************************************ PERSONA *****************/
Route::group(['prefix' => 'Persona', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [PersonaController::class, 'list'])->name('Persona.list');
    Route::get('/listPersonaColaborador', [PersonaController::class, 'listPersonaColaborador'])->name('Persona.listPersonaColaborador');
    Route::get('/index', [PersonaController::class, 'index'])->name('Persona.index');
    Route::post('/destroy', [PersonaController::class, 'destroy'])->name('Persona.destroy');
    Route::post('/store', [PersonaController::class, 'store'])->name('Persona.store');
    Route::get('/show', [PersonaController::class, 'show'])->name('Persona.show');
    Route::get('/select2', [PersonaController::class, 'select2'])->name('Persona.select2');
    Route::post('/changePassword', [PersonaController::class, 'changePassword'])->name('Persona.changePassword');

});

/************************************ FILIACION *****************/

Route::group(['prefix' => 'Filiacion', 'middleware' => 'auth:api'], function () {

Route::get('/generar', [FiliacionController::class, 'generar'])->name('Filiacion.generar'); 

});

/************************************ TipoReporte *****************/
Route::group(['prefix' => 'TipoReporte', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [TipoReporteController::class, 'list'])->name('TipoReporte.list');
    Route::get('/index', [TipoReporteController::class, 'index'])->name('TipoReporte.index');
    Route::post('/destroy', [TipoReporteController::class, 'destroy'])->name('TipoReporte.destroy');
    Route::post('/store', [TipoReporteController::class, 'store'])->name('TipoReporte.store');
    Route::get('/show', [TipoReporteController::class, 'show'])->name('TipoReporte.show');
    Route::post('/generate', [TipoReporteController::class, 'generate'])->name('TipoReporte.generate');
});




/****************************************** TIPO COMPANIA *********************************/
Route::group(['prefix' => 'TipoCompania', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [TipoCompaniaController::class, 'list'])->name('TipoCompania.list');
    Route::get('/index', [TipoCompaniaController::class, 'index'])->name('TipoCompania.index');
    Route::post('/destroy', [TipoCompaniaController::class, 'destroy'])->name('TipoCompania.destroy');
    Route::post('/store', [TipoCompaniaController::class, 'store'])->name('TipoCompania.store');
    Route::get('/show', [TipoCompaniaController::class, 'show'])->name('TipoCompania.show');
});


/****************************************** TIPO DOCUMENTO *********************************/
Route::group(['prefix' => 'TipoDocumento', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [TipoDocumentoController::class, 'list'])->name('TipoDocumento.list');
    Route::get('/index', [TipoDocumentoController::class, 'index'])->name('TipoDocumento.index');
    Route::post('/destroy', [TipoDocumentoController::class, 'destroy'])->name('TipoDocumento.destroy');
    Route::post('/store', [TipoDocumentoController::class, 'store'])->name('TipoDocumento.store');
    Route::get('/show', [TipoDocumentoController::class, 'show'])->name('TipoDocumento.show');
});

/****************************************** TIPO ASISTENCIA *********************************/
Route::group(['prefix' => 'TipoAsistencia', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [TipoAsistenciaController::class, 'list'])->name('TipoAsistencia.list');
    Route::get('/index', [TipoAsistenciaController::class, 'index'])->name('TipoAsistencia.index');
    Route::post('/destroy', [TipoAsistenciaController::class, 'destroy'])->name('TipoAsistencia.destroy');
    Route::post('/store', [TipoAsistenciaController::class, 'store'])->name('TipoAsistencia.store');
    Route::get('/show', [TipoAsistenciaController::class, 'show'])->name('TipoAsistencia.show');
});

/****************************************** DOCUMENTACION *********************************/
Route::group(['prefix' => 'Documentacion', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [DocumentacionController::class, 'list'])->name('Documentacion.list');
    Route::get('/index', [DocumentacionController::class, 'index'])->name('Documentacion.index');
    Route::post('/destroy', [DocumentacionController::class, 'destroy'])->name('Documentacion.destroy');
    Route::post('/store', [DocumentacionController::class, 'store'])->name('Documentacion.store');
    Route::get('/show', [DocumentacionController::class, 'show'])->name('Documentacion.show');

    Route::post('/revision', [DocumentacionController::class, 'revision'])->name('Documentacion.revision');

    Route::get('/listDocumentosporPersona', [DocumentacionController::class, 'listDocumentosporPersona'])->name('Documentacion.listDocumentosporPersona');

});


/****************************************** COMPANIA *********************************/
Route::group(['prefix' => 'Compania', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [CompaniaController::class, 'list'])->name('Compania.list');
    Route::get('/index', [CompaniaController::class, 'index'])->name('Compania.index');
    Route::post('/destroy', [CompaniaController::class, 'destroy'])->name('Compania.destroy');
    Route::post('/store', [CompaniaController::class, 'store'])->name('Compania.store');
    Route::get('/show', [CompaniaController::class, 'show'])->name('Compania.show');
});


/****************************************** ESCUADRA *********************************/
Route::group(['prefix' => 'Escuadra', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [EscuadraController::class, 'list'])->name('Escuadra.list');
    Route::get('/index', [EscuadraController::class, 'index'])->name('Escuadra.index');
    Route::post('/destroy', [EscuadraController::class, 'destroy'])->name('Escuadra.destroy');
    Route::post('/store', [EscuadraController::class, 'store'])->name('Escuadra.store');
    Route::get('/show', [EscuadraController::class, 'show'])->name('Escuadra.show');
});


/****************************************** SECCION *********************************/
Route::group(['prefix' => 'Seccion', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [SeccionController::class, 'list'])->name('Seccion.list');
    Route::get('/index', [SeccionController::class, 'index'])->name('Seccion.index');
    Route::post('/destroy', [SeccionController::class, 'destroy'])->name('Seccion.destroy');
    Route::post('/store', [SeccionController::class, 'store'])->name('Seccion.store');
    Route::get('/show', [SeccionController::class, 'show'])->name('Seccion.show');
});


/****************************************** ESTADO ASISTENCIA *********************************/
Route::group(['prefix' => 'EstadoAsistencia', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [EstadoAsistenciaController::class, 'list'])->name('EstadoAsistencia.list');
    Route::get('/index', [EstadoAsistenciaController::class, 'index'])->name('EstadoAsistencia.index');
    Route::post('/destroy', [EstadoAsistenciaController::class, 'destroy'])->name('EstadoAsistencia.destroy');
    Route::post('/store', [EstadoAsistenciaController::class, 'store'])->name('EstadoAsistencia.store');
    Route::get('/show', [EstadoAsistenciaController::class, 'show'])->name('EstadoAsistencia.show');
});



/****************************************** ESTADO PERSONA *********************************/
Route::group(['prefix' => 'EstadoPersona', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [EstadoPersonaController::class, 'list'])->name('EstadoPersona.list');
    Route::get('/index', [EstadoPersonaController::class, 'index'])->name('EstadoPersona.index');
    Route::post('/destroy', [EstadoPersonaController::class, 'destroy'])->name('EstadoPersona.destroy');
    Route::post('/store', [EstadoPersonaController::class, 'store'])->name('EstadoPersona.store');
    Route::get('/show', [EstadoPersonaController::class, 'show'])->name('EstadoPersona.show');
});


/****************************************** ESTADO DOCUMENTO *********************************/
Route::group(['prefix' => 'EstadoDocumento', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [EstadoDocumentoController::class, 'list'])->name('EstadoDocumento.list');
    Route::get('/index', [EstadoDocumentoController::class, 'index'])->name('EstadoDocumento.index');
    Route::post('/destroy', [EstadoDocumentoController::class, 'destroy'])->name('EstadoDocumento.destroy');
    Route::post('/store', [EstadoDocumentoController::class, 'store'])->name('EstadoDocumento.store');
    Route::get('/show', [EstadoDocumentoController::class, 'show'])->name('EstadoDocumento.show');
});


/****************************************** COMPANIA-SECCION *********************************/
Route::group(['prefix' => 'CompaniaSeccion', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [CompaniaSeccionController::class, 'list'])->name('CompaniaSeccion.list');
    Route::get('/index', [CompaniaSeccionController::class, 'index'])->name('CompaniaSeccion.index');
    Route::post('/destroy', [CompaniaSeccionController::class, 'destroy'])->name('CompaniaSeccion.destroy');
    Route::post('/store', [CompaniaSeccionController::class, 'store'])->name('CompaniaSeccion.store');
    Route::get('/show', [CompaniaSeccionController::class, 'show'])->name('CompaniaSeccion.show');
});



/****************************************** COMPANIA-SECCION-ESCUADRA *********************************/
Route::group(['prefix' => 'CompaniaSeccionEscuadra', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [CompaniaSeccionEscuadraController::class, 'list'])->name('CompaniaSeccionEscuadra.list');
    Route::get('/index', [CompaniaSeccionEscuadraController::class, 'index'])->name('CompaniaSeccionEscuadra.index');
    Route::post('/destroy', [CompaniaSeccionEscuadraController::class, 'destroy'])->name('CompaniaSeccionEscuadra.destroy');
    Route::post('/store', [CompaniaSeccionEscuadraController::class, 'store'])->name('CompaniaSeccionEscuadra.store');
    Route::get('/show', [CompaniaSeccionEscuadraController::class, 'show'])->name('CompaniaSeccionEscuadra.show');
});


/****************************************** COMPANIA-SECCION-ESCUADRA-PERSONA *********************************/
Route::group(['prefix' => 'CompSecEscPersona', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [CompSecEscPersonaController::class, 'list'])->name('CompSecEscPersona.list');
    Route::get('/index', [CompSecEscPersonaController::class, 'index'])->name('CompSecEscPersona.index');
    Route::post('/destroy', [CompSecEscPersonaController::class, 'destroy'])->name('CompSecEscPersona.destroy');
    Route::post('/store', [CompSecEscPersonaController::class, 'store'])->name('CompSecEscPersona.store');
    Route::get('/show', [CompSecEscPersonaController::class, 'show'])->name('CompSecEscPersona.show');
    Route::get('/showEstado', [CompSecEscPersonaController::class, 'showEstado'])->name('CompSecEscPersona.showEstado');
    Route::post('/storeEstado', [CompSecEscPersonaController::class, 'storeEstado'])->name('CompSecEscPersona.storeEstado');
    Route::get('/showInfoCefor', [CompSecEscPersonaController::class, 'showInfoCefor'])->name('CompSecEscPersona.showInfoCefor');
});


/****************************************** ASISTENCIA *********************************/
Route::group(['prefix' => 'Asistencia', 'middleware' => 'auth:api'], function () {
    Route::get('/list', [AsistenciaController::class, 'list'])->name('Asistencia.list');
    Route::get('/index', [AsistenciaController::class, 'index'])->name('Asistencia.index');
    Route::post('/destroy', [AsistenciaController::class, 'destroy'])->name('Asistencia.destroy');
    Route::post('/store', [AsistenciaController::class, 'store'])->name('Asistencia.store');

    Route::post('/storeRevista', [AsistenciaController::class, 'storeRevista'])->name('Asistencia.storeRevista');
    Route::get('/show', [AsistenciaController::class, 'show'])->name('Asistencia.show');
});

/****************************************** CERTIFICACION *********************************/
Route::group(['prefix' => 'Certificado', 'middleware' => 'auth:api'], function () {
    
    Route::get('/index', [CertificadoController::class, 'index'])->name('Certificado.index');
    Route::get('/generar', [CertificadoController::class, 'generar'])->name('Certificado.generar'); 

    
});

