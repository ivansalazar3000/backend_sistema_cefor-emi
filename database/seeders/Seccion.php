<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Seccion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Seccion')->insert(['NombreSeccion'=>'Primera Seccion compañia A', 'Descripcion' => 'Primera seccion para la compañia A']);
        DB::table('Seccion')->insert(['NombreSeccion'=>'Segunda Seccion compañia A', 'Descripcion' => 'Segunda seccion para la compañia A']);
        DB::table('Seccion')->insert(['NombreSeccion'=>'Tercera Seccion compañia A', 'Descripcion' => 'Tercera seccion para la compañia A']);
        DB::table('Seccion')->insert(['NombreSeccion'=>'Primera Seccion compañia B', 'Descripcion' => 'Primera seccion para la compañia B']);
        DB::table('Seccion')->insert(['NombreSeccion'=>'Segunda Seccion compañia B', 'Descripcion' => 'Segunda seccion para la compañia B']);
        DB::table('Seccion')->insert(['NombreSeccion'=>'Tercera Seccion compañia B', 'Descripcion' => 'Tercera seccion para la compañia B']);

    }
}
