<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoDocumento extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('EstadoDocumento')->insert(['EstadoDocumento'=>'En Revision', 'Descripcion' => 'Documento en Revision']);
        DB::table('EstadoDocumento')->insert(['EstadoDocumento'=>'Observado', 'Descripcion' => 'Documento Observado']);
        DB::table('EstadoDocumento')->insert(['EstadoDocumento'=>'Aceptado', 'Descripcion' => 'Documento Aceotado']);
        

    }
}
