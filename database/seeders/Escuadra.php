<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Escuadra extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Escuadra')->insert(['NombreEscuadra'=>'Primera escuadra', 'Descripcion' => 'Primera escuadra para la primera seccion Compañia A']);
        DB::table('Escuadra')->insert(['NombreEscuadra'=>'Segunda escuadra', 'Descripcion' => 'Segunda escuadra para la primera seccion Compañia A']);
        DB::table('Escuadra')->insert(['NombreEscuadra'=>'Tercera escuadra', 'Descripcion' => 'Tercera escuadra para la primera seccion Compañia A']);
        DB::table('Escuadra')->insert(['NombreEscuadra'=>'Primera escuadra', 'Descripcion' => 'Primera escuadra para la segunda seccion Compañia A']);
        DB::table('Escuadra')->insert(['NombreEscuadra'=>'Segunda escuadra', 'Descripcion' => 'Segunda escuadra para la segunda seccion Compañia A']);
        DB::table('Escuadra')->insert(['NombreEscuadra'=>'Tercera escuadra', 'Descripcion' => 'Tercera escuadra para la segunda seccion Compañia A']);

    }
}
