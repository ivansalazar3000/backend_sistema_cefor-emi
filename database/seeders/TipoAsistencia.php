<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoAsistencia extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('TipoAsistencia')->insert(['TipoAsistencia'=>'Asiste', 'Descripcion' => 'El estudiante asistio a la actividad de instruccion militar.']);
        DB::table('TipoAsistencia')->insert(['TipoAsistencia'=>'Falta', 'Descripcion' => 'El estudiante falto a la actividad de instruccion militar.']);
        DB::table('TipoAsistencia')->insert(['TipoAsistencia'=>'Permiso', 'Descripcion' => 'El estudiante tiene permiso a la actividad de instruccion militar.']);
        

    }
}
