<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoDocumento extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('TipoDocumento')->insert(['TipoDocumento'=>'Certificado de Nacimiento', 'Descripcion' => 'Documento Oficial como Certificados de nacimiento el cual se encuentra dentro de los documentos categorizados como oficial.']);
        DB::table('TipoDocumento')->insert(['TipoDocumento'=>'Carnet de Identidad', 'Descripcion' => 'Documento de Identidad como el carnet de identidad que contiene datos de identificación personal.']);
        DB::table('TipoDocumento')->insert(['TipoDocumento'=>'Certificado Medico personal', 'Descripcion' => 'Certificados médicos, análisis clínico del tipo sanguíneo; el cual son catalogados como un documento médico-legal.']);
        DB::table('TipoDocumento')->insert(['TipoDocumento'=>'Croquis de localización', 'Descripcion' => 'Los documentos que cuentan con una representación de ubicaciones de un área en un mapa a pequeña escala como los croquis de referencia personales, estará categorizado como un documento cartográfico.']);
        DB::table('TipoDocumento')->insert(['TipoDocumento'=>'Documento de Solicitud de Permiso', 'Descripcion' => 'Los documentos dentro de esta categoría están las cartas de solicitud de permisos, justificaciones médicas, laborales entre otros.']);
        DB::table('TipoDocumento')->insert(['TipoDocumento'=>'Licencia de conducir', 'Descripcion' => 'El documento de Licencia de Conducir es un documento público individual, se constituye en una autorización administrativa a su poseedor para la conducción de vehículos terrestres por la vía pública.']);

    }
}
