<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoPersona extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //DB::table('EstadoPersona')->insert(['EstadoPersona'=>'Habilitado', 'Descripcion' => 'Persona habilitado']);
        DB::table('EstadoPersona')->insert(['EstadoPersona'=>'En instruccion', 'Descripcion' => 'Persona que asiste a Instruccion Militar de manera normal']);
        DB::table('EstadoPersona')->insert(['EstadoPersona'=>'En observacion', 'Descripcion' => 'Persona observado']);
        DB::table('EstadoPersona')->insert(['EstadoPersona'=>'No habilitado', 'Descripcion' => 'Persona no habilitado']);
        DB::table('EstadoPersona')->insert(['EstadoPersona'=>'Habilitado a la certificacion', 'Descripcion' => 'Persona habilitada a obtener el certificado CEFOR']);

    }
}
