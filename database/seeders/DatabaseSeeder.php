<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UnidadAcademica::class);
        $this->call(Rol::class);
        $this->call(Persona::class);
        $this->call(TipoReporte::class);
        $this->call(TipoDocumento::class);
        $this->call(TipoAsistencia::class);
        $this->call(EstadoDocumento::class);
        $this->call(EstadoPersona::class);
        $this->call(Escuadra::class);
        $this->call(Seccion::class);
        

        // \App\Models\User::factory(10)->create();
    }
}
