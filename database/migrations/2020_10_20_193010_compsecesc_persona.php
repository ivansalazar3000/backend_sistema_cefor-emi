<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompSecEscPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('CompSecEscPersona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('CompaniaSeccionEscuadra')->unsigned()->nullable();
            $table->integer('Persona')->unsigned()->nullable();
            $table->integer('EstadoPersona')->unsigned()->nullable();
            $table->integer('CantidadAsistencia')->nullable();
            $table->integer('CantidadFalta')->nullable();
            $table->integer('CantidadPermiso')->nullable();
            $table->integer('RevistaMilitar')->nullable();
            
            

            /*

            credenciales de acceso al sistema 
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->boolean('Activo')->default(false);
            $table->string('TokenLogin')->nullable();
            $table->rememberToken();



             campos para login con Office365 
            $table->datetime('UltimoInicioSesion')->nullable();
            $table->string('SocialLogin', 50)->nullable();
            $table->string('SocialLoginId', 150)->nullable();
            $table->string('Avatar', 250)->nullable();

            $table->string('Office365Id', 150)->nullable();

            */

            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();

            $table->foreign('CompaniaSeccionEscuadra')->references('id')->on('CompaniaSeccionEscuadra');
            $table->foreign('Persona')->references('id')->on('Persona');
            $table->foreign('EstadoPersona')->references('id')->on('EstadoPersona');
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CompSecEscPersona'); //
    }
}
