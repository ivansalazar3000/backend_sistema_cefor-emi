<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Persona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Persona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('UnidadAcademica')->unsigned()->nullable();
            $table->integer('Rol')->unsigned()->nullable();
            $table->string('ApellidoPaterno',50)->nullable();
            $table->string('ApellidoMaterno',50)->nullable();
            $table->string('Nombres',50);
            $table->string('Persona',650);
            $table->string('Foto', 250)->nullable();

            /*campos incorporados de una persona */
            $table->string('Ciudad', 50)->nullable();
            $table->integer('Edad')->nullable();
            $table->string('Carrera', 50)->nullable();
            $table->string('CodigoSaga', 50)->nullable();
            $table->string('CorreoInstitucional', 50)->nullable();
            $table->date('FechaRegistro')->nullable();
            $table->date('FechaNacimiento')->nullable();
            $table->string('Direccion', 50)->nullable();
            $table->string('NumeroCelular', 50)->nullable();
            $table->string('TelefonoReferencia', 50)->nullable();
            $table->string('Ci')->nullable();
            $table->string('NombrePadre', 50)->nullable();
            $table->string('NombreMadre', 50)->nullable();
            $table->string('Nadar', 50)->nullable();
            $table->string('Montar', 50)->nullable();
            $table->string('Escribir', 50)->nullable();
            $table->string('Leer', 50)->nullable();
            $table->string('ConduceVehiculo', 50)->nullable();
            $table->string('ConduceEmbarcacion', 50)->nullable(); 
            
            /*campos filiacion */

            $table->string('RecMedico', 50)->nullable();
            $table->float('Estatura')->nullable(); 
            $table->integer('Peso')->nullable();
            $table->string('CondFisica', 50)->nullable();
            $table->string('EstNutricional', 50)->nullable();
            $table->string('GrupoSanguineo', 50)->nullable();
            $table->string('ColorPiel', 50)->nullable();
            $table->string('ColorOjos', 50)->nullable();
            $table->string('Nariz', 50)->nullable();
            $table->string('Boca', 50)->nullable();  
            $table->string('Labio', 50)->nullable();
            $table->string('Cabello', 50)->nullable();  

            


            /* credenciales de acceso al sistema */
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->boolean('Activo')->default(false);
            $table->string('TokenLogin')->nullable();
            $table->rememberToken();



            /* campos para login con Office365 */
            $table->datetime('UltimoInicioSesion')->nullable();
            $table->string('SocialLogin', 50)->nullable();
            $table->string('SocialLoginId', 150)->nullable();
            $table->string('Avatar', 250)->nullable();

            $table->string('Office365Id', 150)->nullable();

            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();

            $table->foreign('UnidadAcademica')->references('id')->on('UnidadAcademica');
            $table->foreign('Rol')->references('id')->on('Rol');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Persona'); //
    }
}
