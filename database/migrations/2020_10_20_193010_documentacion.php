<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Documentacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Documentacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Persona')->unsigned()->nullable();
            $table->string('Documento')->nullable();
            $table->integer('TipoDocumento')->unsigned()->nullable();
            $table->integer('EstadoDocumento')->unsigned()->nullable();
            $table->string('FechaEntrega')->nullable();
            
            

            /*

            credenciales de acceso al sistema 
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->boolean('Activo')->default(false);
            $table->string('TokenLogin')->nullable();
            $table->rememberToken();



             campos para login con Office365 
            $table->datetime('UltimoInicioSesion')->nullable();
            $table->string('SocialLogin', 50)->nullable();
            $table->string('SocialLoginId', 150)->nullable();
            $table->string('Avatar', 250)->nullable();

            $table->string('Office365Id', 150)->nullable();

            */

            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();

            
            $table->foreign('Persona')->references('id')->on('Persona');
            $table->foreign('EstadoDocumento')->references('id')->on('EstadoDocumento');
            $table->foreign('TipoDocumento')->references('id')->on('TipoDocumento');
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Documentacion'); //
    }
}
