<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Compania;
use App\Models\Seccion;
use App\Models\Escuadra;
use App\Models\CompaniaSeccion;
use App\Models\CompaniaSeccionEscuadra;
use App\Models\Persona;
use App\Models\EstadoPersona;



class CompSecEscPersona extends Model
{
    use HasFactory;
    protected $table = 'CompSecEscPersona';



    
    public function companiaSeccionEscuadra() {
        return $this->belongsTo(CompaniaSeccionEscuadra::class, 'CompaniaSeccionEscuadra');
    }

    public function persona() {
        return $this->belongsTo(Persona::class, 'Persona');
    }


    public function estadoPersona() {
        return $this->belongsTo(EstadoPersona::class, 'EstadoPersona');
    }
    
    

    



}
