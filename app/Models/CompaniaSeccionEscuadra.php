<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\CompaniaSeccion;
use App\Models\Escuadra;
use App\Models\Persona;



class CompaniaSeccionEscuadra extends Model
{
    use HasFactory;
    protected $table = 'CompaniaSeccionEscuadra';



    public function escuadra() {
        return $this->belongsTo(Escuadra::class, 'Escuadra');
    }
    

    public function personas(){                    //nombre tabla     foreign tabla   foreign relacion   
        return $this->belongsToMany(Persona::class, 'CompSecEscPersona', 'CompaniaSeccionEscuadra' , 'Persona');
    }
    

    public function companiaSeccion(){                    //nombre tabla     foreign tabla   foreign relacion   
        return $this->belongsTo(CompaniaSeccion::class, 'CompaniaSeccion');
    }
    



}
