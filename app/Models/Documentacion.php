<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Persona;
use App\Models\EstadoDocumento;
use App\Models\TipoDocumento;



class Documentacion extends Model
{
    use HasFactory;
    protected $table = 'Documentacion';


    protected $appends = ['URLDocumento'];


    public function persona() {
        return $this->belongsTo(Persona::class, 'Persona');
    }

    public function tipoDocumento() {
        return $this->belongsTo(TipoDocumento::class, 'TipoDocumento');
    }


    public function getURLDocumentoAttribute() {
        if($this->Documento) {
            return url('/') . '/storage/documents/documents/' . $this->Documento;
        } else {
            return url('/') . '/images/default_image_profile.png';
        }

    }
}
