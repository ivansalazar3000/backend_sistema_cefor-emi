<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\TipoCompania;
use App\Models\Seccion;
use App\Models\Persona;

class Compania extends Model
{
    use HasFactory;
    protected $table = 'Compania';

    /*relacion de uno a uno con tipo de compañia */
    public function tipoCompania() {   //            aca el campo con llave secundaria de la tabla compañia
        return $this->belongsTo(TipoCompania::class, 'Compania');
    }

    
    public function persona() {
        
        return $this->belongsTo(Persona::class, 'ComandanteCompania');
    }
    

    //relacion de muchos a muchos

    public function secciones(){                    //nombre tabla     foreign tabla   foreign relacion   
        return $this->belongsToMany(Seccion::class, 'CompaniaSeccion', 'Compania' , 'Seccion');
    }

} 
