<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\CompaniaSeccion;

class Escuadra extends Model
{
    use HasFactory;
    protected $table = 'Escuadra';


    public function companiasSecciones(){
        return $this->belongsToMany(CompaniaSeccion::class, 'CompaniaSeccionEscuadra', 'Escuadra', 'CompaniaSeccion');
    }



}
