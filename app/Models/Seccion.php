<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Compania;

class Seccion extends Model
{
    use HasFactory;
    protected $table = 'Seccion';




    public function companias(){
        return $this->belongsToMany(Compania::class, 'CompaniaSeccion', 'Seccion', 'Compania');
    }
}
