<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Seccion;
use App\Models\Compania;
use App\Models\Escuadra;



class CompaniaSeccion extends Model
{
    use HasFactory;
    protected $table = 'CompaniaSeccion';


    
    public function compania() {
        return $this->belongsTo(Compania::class, 'Compania');
    }

    public function seccion() {
        return $this->belongsTo(Seccion::class, 'Seccion');
    }


    
    public function escuadras(){
        return $this->belongsToMany(Escuadra::class, 'CompaniaSeccionEscuadra', 'CompaniaSeccion', 'Escuadra');
    }

    


    



}
