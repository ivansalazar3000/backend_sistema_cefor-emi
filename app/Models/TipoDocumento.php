<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Persona;

class TipoDocumento extends Model
{
    use HasFactory;
    protected $table = 'TipoDocumento';

    //Persona_documento

    public function personas(){
        return $this->belongsToMany(Personas::class, 'Documentacion', 'TipoDocumento', 'Persona');
    }


}
