<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



use App\Models\CompSecEscPersona;

class EstadoPersona extends Model
{
    use HasFactory;
    protected $table = 'EstadoPersona';


    public function companiaSeccionEscuadrasPersonas(){                    //nombre tabla     foreign tabla   foreign relacion   
        return $this->belongsToMany(CompSecEscPersona::class, 'CompSecEscPersona', 'EstadoPersona');
    }

}
