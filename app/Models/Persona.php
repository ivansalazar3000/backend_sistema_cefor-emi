<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use App\Models\CompaniaSeccionEscuadra;
use App\Models\CompSecEscPersona;
use App\Models\Documentacion;

class Persona extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'Persona';

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['URLFoto'];

    public function unidadAcademica() {
        return $this->belongsTo(UnidadAcademica::class, 'UnidadAcademica');
    }
    
    public function rol() {
        return $this->belongsTo(Rol::class, 'Rol');
    }

    public function companiaSeccionEscuadras(){                    //nombre tabla     foreign tabla   foreign relacion   
        return $this->belongsToMany(CompSecEscPersona::class, 'CompSecEscPersona', 'Persona' , 'CompaniaSeccionEscuadra');
    }

    //relacion de muchos a muchos con documentacion
    public function documentaciones(){                    //nombre tabla     foreign tabla   foreign relacion   
        return $this->belongsToMany(Documentacion::class, 'Documentacion', 'Persona', 'TipoDocumento');
    }



    public function getURLFotoAttribute() {
        if($this->Foto) {
            return url('/') . '/storage/documents/documents/' . $this->Foto;
        } else {
            return url('/') . '/images/default_image_profile.png';
        }
    }
}







