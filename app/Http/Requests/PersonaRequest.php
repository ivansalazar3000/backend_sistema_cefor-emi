<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PersonaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ($request->id) {
            return [
                'Nombres' => 'required|regex:/(^[A-Za-z ]+$)+/|max:50',
                'ApellidoPaterno' => 'nullable|max:80',
                'ApellidoMaterno' => 'nullable|max:80',
                'UnidadAcademica' => 'required',
                'Rol' => 'required',
                'email' =>  'required|email|unique:Persona',

                'Ciudad' => 'required_if:Rol,3',
                'Edad' => 'required_if:Rol,3',
                'Carrera' => 'required_if:Rol,3',
                'CodigoSaga' => 'required_if:Rol,3|unique:Persona',
                'CorreoInstitucional' => 'required_if:Rol,3',
                'FechaNacimiento' => 'required_if:Rol,3',
                'Direccion' => 'required_if:Rol,3',
                'NumeroCelular' => 'required_if:Rol,3',
                'TelefonoReferencia' => 'required_if:Rol,3',
                'NumeroCi' => 'required_if:Rol,3',
                'ExtCiudad' => 'required_if:Rol,3',
                
                'NombrePadre' => 'required_if:Rol,3',
                'NombreMadre' => 'required_if:Rol,3',
                'Nadar' => 'required_if:Rol,3',
                'Montar' => 'required_if:Rol,3',
                
                'Leer' => 'required_if:Rol,3',
                'ConduceVehiculo' => 'required_if:Rol,3',
                'ConduceEmbarcacion' => 'required_if:Rol,3',
                'RecMedico' => 'required_if:Rol,3',
                'Estatura' => 'required_if:Rol,3',
                'Peso' => 'required_if:Rol,3',
                'CondFisica' => 'required_if:Rol,3',
                'EstNutricional' => 'required_if:Rol,3',
                'GrupoSanguineo' => 'required_if:Rol,3',
                'ColorPiel'  => 'required_if:Rol,3',
                'ColorOjos'  => 'required_if:Rol,3',
                'Nariz'  => 'required_if:Rol,3',
                'Boca'  => 'required_if:Rol,3', 
                'Labio'  => 'required_if:Rol,3',
                'Cabello'  => 'required_if:Rol,3',
                

            ];
        } else {
            return [
                'Nombres' => 'required|regex:/(^[A-Za-z ]+$)+/|max:50',
                'ApellidoPaterno' => 'required|alpha|max:50',
                'ApellidoMaterno' => 'nullable|max:50',
                'UnidadAcademica' => 'required',
                'Rol' => 'required',
                'email' => 'required|email|unique:Persona',

                'Ciudad' => 'required_if:Rol,3',
                'Edad' => 'required_if:Rol,3',
                'Carrera' => 'required_if:Rol,3',
                'CodigoSaga' => 'required_if:Rol,3|unique:Persona',
                'CorreoInstitucional' => 'required_if:Rol,3|email|unique:Persona',
                'FechaNacimiento' => 'required_if:Rol,3',
                'Direccion' => 'required_if:Rol,3',
                'NumeroCelular' => 'required_if:Rol,3',
                'TelefonoReferencia' => 'required_if:Rol,3',
                'NumeroCi' => 'required_if:Rol,3',
                'ExtCiudad' => 'required_if:Rol,3',
                
                'NombrePadre' => 'required_if:Rol,3',
                'NombreMadre' => 'required_if:Rol,3',
                'Nadar' => 'required_if:Rol,3',
                'Montar' => 'required_if:Rol,3',
                
                'Leer' => 'required_if:Rol,3',
                'ConduceVehiculo' => 'required_if:Rol,3',
                'ConduceEmbarcacion' => 'required_if:Rol,3',
                'RecMedico' => 'required_if:Rol,3',
                'Estatura' => 'required_if:Rol,3',
                'Peso' => 'required_if:Rol,3',
                'CondFisica' => 'required_if:Rol,3',
                'EstNutricional' => 'required_if:Rol,3',
                'GrupoSanguineo' => 'required_if:Rol,3',
                'ColorPiel'  => 'required_if:Rol,3',
                'ColorOjos'  => 'required_if:Rol,3',
                'Nariz'  => 'required_if:Rol,3',
                'Boca'  => 'required_if:Rol,3', 
                'Labio'  => 'required_if:Rol,3',
                'Cabello'  => 'required_if:Rol,3',


            ];
        }
    }
}
