<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Models\Documentacion;
use App\Models\EstadoDocumento;
use App\Models\TipoDocumento;
use App\Http\Requests\DocumentacionRequest;

class DocumentacionController extends Controller
{
    //
    public function index()
    {
        
        $documentacion = Documentacion::from('Documentacion as d')
                ->leftJoin('Persona as p', 'd.Persona', '=', 'p.id')
                ->leftJoin('TipoDocumento as td', 'd.TipoDocumento', '=', 'td.id')
                ->leftJoin('EstadoDocumento as ed', 'd.EstadoDocumento', '=', 'ed.id')
                ->whereNull('d.deleted_at')
                ->select('d.id', 'p.Persona' ,'d.Documento','td.TipoDocumento','ed.EstadoDocumento', 'd.FechaEntrega');

        if (Auth::user()->Rol == 3) //estudiante
            $documentacion->where('d.Persona', Auth::user()->id);

        return Datatables::of($documentacion)
            ->addIndexColumn()
            ->addColumn('action', function ($p) {
                return '<a class="btn btn-info btn-xs btn-datatable-Documentacion" id="' . $p->id . '"><i class="fa fa-bars"></i> ' . 'Detalles' . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }


    public function listDocumentosporPersona(Request $request)
    {

        $item = Documentacion::where('Persona', $request->id)->first();
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );
        


        return response()->json($data);
    }






    public function list(Request $request)
    {

        $item = new Documentacion();
        $objeto = null;

        $objeto = $item->orderBy('id', 'asc')->whereNull('deleted_at')->get();

        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }


    public function show(Request $request)
    {
        $item = Documentacion::findOrFail($request->id);
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }



    public function store(DocumentacionRequest $request)
    {
        if ($request->id) {
            $item = Documentacion::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new Documentacion();
            $item->CreatorUserName = Auth::user()->email;
            $item->CreatorFullUserName = Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Persona = $request->Persona;
        $item->Documento = $request->Documento;
        $item->TipoDocumento = $request->TipoDocumento;
        $item->EstadoDocumento = $request->EstadoDocumento;
        $item->FechaEntrega =  $request->FechaEntrega;

        $item->UpdaterUserName = Auth::user()->email;
        $item->UpdaterFullUserName = Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }


    public function revision(Request $request){

        if ($request->id) {
            $item = Documentacion::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new Documentacion();
            $item->CreatorUserName = Auth::user()->email;
            $item->CreatorFullUserName = Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Persona = $request->Persona;
        $item->Documento = $request->Documento;
        $item->TipoDocumento = $request->TipoDocumento;
        $item->EstadoDocumento = $request->EstadoDocumento;
        $item->FechaEntrega =  $request->FechaEntrega;

        $item->UpdaterUserName = Auth::user()->email;
        $item->UpdaterFullUserName = Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);

    }

    

    public function destroy(Request $request)
    {
        $item = Documentacion::where('id', $request->id)->first();
        $item->deleted_at = Carbon::now();
        $item->DeleterUserName = Auth::user()->Persona;
        $item->DeleterFullUserName = Auth::user()->Persona;
        $item->DeleterIP =  $request->ip();
        $item->save();
        $result = array(
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );

        return response()->json($result);
    }
}
