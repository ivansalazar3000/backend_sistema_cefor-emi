<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Models\Persona;
use App\Http\Requests\PersonaRequest;
use Illuminate\Support\Str;
use JasperPHP\Facades\JasperPHP;
//use PDF;

class FiliacionController extends Controller
{
    //
    protected $basePathGenerated;
    protected $urlFile;

    public function getDatabaseConfig(){

        
        $jdbc_dir = 'C:\Users\WINDOWS\Desktop\Proyecto\starterv-2-backend\vendor\cossou\jasperphp\src\JasperStarter\jdbc';

        return[
            'driver'        => 'generic',
            'host'          => env('DB_HOST'),
            'port'          => env('DB_PORT'),
            'username'      => env('DB_USERNAME'),
            'password'      => env('DB_PASSWORD'),
            'database'      => env('DB_DATABASE'),
            'jdbc_driver'   => 'com.mysql.jdbc.Driver',
            'jdbc_url'      => 'jdbc:mysql://localhost/'.env('DB_DATABASE'),
            'jdbc_dir'      => $jdbc_dir

        ];

    }

    /***********************METODO PARA FILIACION****************************/
    public function generar(Request $request)
    {

        $item = Persona::where('id', $request->id)->with('unidadAcademica', 'rol')->first();

        $foto = Persona::select('Foto')->where('id', $request->id)->first();
       
        
        //return ($foto);

        $extension = 'pdf';
        $nombre = 'filiacionCefor';
        $filenombre = $nombre . time();
        $output = base_path('public/tmp/'. $filenombre);
        $this->basePathGenerated = public_path('tmp/');
        $this->urlFile = config('app.url') . 'tmp/';
        JasperPHP::compile(storage_path('app/public').'/hojaFiliacion/hojaFiliacion.jrxml')->execute();

        $params = array('urlLogo' => public_path('images/emi_logo.png'),
                        'FotoPersona' => public_path('storage/documents/documents/'.$foto->Foto) 
        );
        $params['IdPersona'] = $request->id;
        //$params['FotoPersona'] = public_path('storage/documents/documents/' . $foto);

        $reporteJasper = JasperPHP::process(
            
            storage_path('app/public/hojaFiliacion/hojaFiliacion.jasper'),
            $output,
            
            array($extension),
            $params,
            //array('IdPersona' => $request->id),
            $this->getDatabaseConfig(),
                
        );

        $reporteJasper->execute();
        
        //$file = $output .'.'.$extension;
        /*
        if(!file_exists($file)){
            abort(404);
        }
        */
        $pdf = array(
            'url' => $this->urlFile . $filenombre .'.pdf',
            'uri' => $this->basePathGenerated . $filenombre . '.pdf',
            'fileNombre' => $filenombre . '.pdf'
        );

        $data = array(
            'success' => true,
            'data' => $pdf,
            'msg' => 'Hoja de Filiacion Generado Correctamente'
        );
        //return response()->file($file)->deleteFileAfterSend();
        return response()->json($data);
        


    }

         

}
