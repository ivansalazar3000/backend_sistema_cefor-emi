<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Models\CompSecEscPersona;
use App\Models\CompaniaSeccionEscuadra;
use App\Models\CompaniaSeccion;
use App\Models\Asistencia;
use App\Models\EstadoAsistencia;
use App\Models\TipoAsistencia;
use App\Models\Escuadra;
use App\Models\Seccion;
use App\Models\Compania;


class AsistenciaController extends Controller
{
    //
    public function index()
    {
        
        $asistencia = CompaniaSeccionEscuadra::from('CompaniaSeccionEscuadra as cse')
                    ->Join('CompaniaSeccion as cs', 'cse.CompaniaSeccion', '=', 'cs.id')

                    ->Join('CompSecEscPersona as csep', 'csep.CompaniaSeccionEscuadra', '=', 'cse.id')

                    ->Join('Escuadra as e', 'cse.Escuadra', '=', 'e.id')

                    ->Join('Compania as c', 'cs.Compania', '=', 'c.id')
                    ->Join('Seccion as s', 'cs.Seccion', '=', 's.id')
                
                    ->whereNull('cse.deleted_at')
                    ->select('cse.id','e.NombreEscuadra','s.NombreSeccion', 'c.NombreCompania','c.Gestion','c.Periodo')
                    ->distinct();



        return Datatables::of($asistencia)
            ->addIndexColumn()
            ->addColumn('action', function ($p) {
                return '<a class="btn btn-info btn-xs btn-datatable-Asistencia" id="' . $p->id . '"><i class="fa fa-bars"></i> ' . 'Detalles' . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }


    /***

    public function list(Request $request)
    {
        $item = new Documentacion();
        $objeto = null;

        $objeto = $item->orderBy('id', 'asc')->whereNull('deleted_at')->get();

        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }
    ***/


    /***
    public function show(Request $request)
    {
        $item = Documentacion::findOrFail($request->id);
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
        
         foreach($per in $request)
    }

    **/


    

    public function store(Request $request)
    {
        // $persona = $request->all();
        // return $request->persona;
    
        foreach($request->persona as $key => $personas)
        {


            
            if ($request->id) {
                $item = Asistencia::findOrFail($request->id);
                $msg = trans('messages.updated');
            } else {
                
                $item = new Asistencia();
                $item->CreatorUserName = Auth::user()->email;
                $item->CreatorFullUserName = Auth::user()->Persona;
                $item->CreatorIP = $request->ip();
                $msg = trans('messages.added');
            }
    


            $item->EscPersona = $personas['id'];
            $item->FechaAsistencia = Carbon::now();
            $item->TipoAsistencia = $personas['TipoAsistencia'];
            //item->EstadoAsistencia = $personas->EstadoAsistencia;
            $item->UpdaterUserName = Auth::user()->email;
            $item->UpdaterFullUserName = Auth::user()->Persona;
            $item->UpdaterIP = $request->ip();
            $item->save();
    
            /*asistencia 1*/
            if($personas['TipoAsistencia'] == 1){
                 $asistencias = CompSecEscPersona::where('id', $personas['id'])->value('CantidadAsistencia');
                 $asistencias++;
                 $totalAsistencias=$asistencias;
                 CompSecEscPersona::where('id',$personas['id'])->update(['CantidadAsistencia' => $totalAsistencias]);
            }



            /*faltas 2*/
            if($personas['TipoAsistencia'] == 2){
                $faltas = CompSecEscPersona::where('id', $personas['id'])->value('CantidadFalta');
                $faltas++;
                $totalFaltas=$faltas;
                CompSecEscPersona::where('id',$personas['id'])->update(['CantidadFalta' => $totalFaltas]);

                if($totalFaltas == 2){
                    $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                    $estado++;
                    $estadoNuevo=$estado;
                    if($estadoNuevo==4){
                        $estadoNuevo=3;
                    }
                    CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);

                    }
                    else{
                        if($totalFaltas == 3){
                            $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                            $estado++;
                            $estadoNuevo=$estado;
                            if($estadoNuevo==4){
                                $estadoNuevo=3;
                            }
                            CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
                        }
                    }   
             }

           

           /*permisos 3*/
           if($personas['TipoAsistencia'] == 3){
            $permisos = CompSecEscPersona::where('id', $personas['id'])->value('CantidadPermiso');
            $permisos++;
            $totalPermisos=$permisos;
            CompSecEscPersona::where('id',$personas['id'])->update(['CantidadPermiso' => $totalPermisos]);

            if($totalPermisos == 2){
                $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                $estado++;
                $estadoNuevo=$estado;
                if($estadoNuevo==4){
                    $estadoNuevo=3;
                }
                CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
                }
                else{
                    if($totalPermisos == 3){
                        $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                        $estado++;
                        $estadoNuevo=$estado;
                        if($estadoNuevo==4){
                            $estadoNuevo=3;
                        }
                        CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
                    }
              }

          }

        }
        


        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);

        
    }


    public function storeRevista(Request $request)
    {
        
        

        foreach($request->persona as $key => $personas)
        {


            
            if ($request->id) {
                $item = Asistencia::findOrFail($request->id);
                $msg = trans('messages.updated');
            } else {
                
                $item = new Asistencia();
                $item->CreatorUserName = Auth::user()->email;
                $item->CreatorFullUserName = Auth::user()->Persona;
                $item->CreatorIP = $request->ip();
                $msg = trans('messages.added');
            }
    


            $item->EscPersona = $personas['id'];
            $item->FechaAsistencia = Carbon::now();
            $item->TipoAsistencia = $personas['TipoAsistencia'];
            //item->EstadoAsistencia = $personas->EstadoAsistencia;
            //$item->RevistaMilitar = $personas['RevistaMilitar'];
            $item->UpdaterUserName = Auth::user()->email;
            $item->UpdaterFullUserName = Auth::user()->Persona;
            $item->UpdaterIP = $request->ip();
            $item->save();
    
            

            /********************CONTROL DE REVISTAS*******************************************/
            if( $personas['RevistaMilitar']<=1){
                $revistas = CompSecEscPersona::where('id', $personas['id'])->value('RevistaMilitar');
                $revistas++;
                $totalRevistas=$revistas;
                CompSecEscPersona::where('id',$personas['id'])->update(['RevistaMilitar' => $totalRevistas]);
           }

           if($personas['RevistaMilitar']==1){
            $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
            $estado = 4;//Habilitado a certificacion
            $estadoNuevo=$estado;
            CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
            }
            /**********************************************************************************/

            /*asistencia 1*/
            if($personas['TipoAsistencia'] == 1){
                 $asistencias = CompSecEscPersona::where('id', $personas['id'])->value('CantidadAsistencia');
                 $asistencias++;
                 $totalAsistencias=$asistencias;
                 CompSecEscPersona::where('id',$personas['id'])->update(['CantidadAsistencia' => $totalAsistencias]);
            }
            /*faltas 2*/
            if($personas['TipoAsistencia'] == 2){
                $faltas = CompSecEscPersona::where('id', $personas['id'])->value('CantidadFalta');
                $faltas++;
                $totalFaltas=$faltas;
                CompSecEscPersona::where('id',$personas['id'])->update(['CantidadFalta' => $totalFaltas]);
                if($totalFaltas == 2){
                    $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                    $estado++;
                    $estadoNuevo=$estado;
                    CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
                    }
                    else{
                        if($totalFaltas == 3){
                            $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                            $estado++;
                            $estadoNuevo=$estado;
                            //
                            CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
                        }
                 }

            }



           /*permisos 3*/
           if($personas['TipoAsistencia'] == 3){
            $permisos = CompSecEscPersona::where('id', $personas['id'])->value('CantidadPermiso');
            $permisos++;
            $totalPermisos=$permisos;
            CompSecEscPersona::where('id',$personas['id'])->update(['CantidadPermiso' => $totalPermisos]);
            if($totalPermisos == 2){
                $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                $estado++;
                $estadoNuevo=$estado;
                CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
                }
                else{
                    if($totalPermisos == 3){
                        $estado = CompSecEscPersona::where('id', $personas['id'])->value('EstadoPersona');
                        $estado++;
                        $estadoNuevo=$estado;
                        CompSecEscPersona::where('id',$personas['id'])->update(['EstadoPersona' => $estadoNuevo]);
                    }
                }

            }

        }
        


        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);

        
    }

   
    

    /***

    public function destroy(Request $request)
    {
        $item = Documentacion::where('id', $request->id)->first();
        $item->deleted_at = Carbon::now();
        $item->DeleterUserName = Auth::user()->Persona;
        $item->DeleterFullUserName = Auth::user()->Persona;
        $item->DeleterIP =  $request->ip();
        $item->save();
        $result = array(
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );

        return response()->json($result);
    }

    ***/
}
