<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Models\Compania;
use App\Http\Requests\CompaniaRequest;

class CompaniaController extends Controller
{
    //
    public function index()
    {
        

        $item = Compania::from('Compania as c')
                ->leftJoin('TipoCompania as tc', 'c.Compania', '=', 'tc.id')
                ->leftJoin('Persona as p', 'c.ComandanteCompania', '=', 'p.id')
                ->whereNull('c.deleted_at')
                ->select('c.id', 'c.NombreCompania' ,'tc.TipoCompania', 'c.Gestion', 'c.Periodo', 'p.Persona');



        return Datatables::of($item)
            ->addIndexColumn()
            ->addColumn('action', function ($p) {
                return '<a class="btn btn-info btn-xs btn-datatable-Compania" id="' . $p->id . '"><i class="fa fa-bars"></i> ' . 'Detalles' . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }




    public function list(Request $request)
    {
        $item = new Compania();
        $objeto = null;

        $objeto = $item->orderBy('id', 'asc')->whereNull('deleted_at')->get();

        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }


    public function show(Request $request)
    {
        $item = Compania::where('id',$request->id)->with('tipoCompania', 'persona')->first();
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }



    public function store(CompaniaRequest $request)
    {
        if ($request->id) {
            $item = Compania::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new Compania();
            $item->CreatorUserName = Auth::user()->email;
            $item->CreatorFullUserName = Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->NombreCompania = $request->NombreCompania;
        $item->Compania = $request->Compania;
        $item->ComandanteCompania = $request->ComandanteCompania;



        $x = Carbon::now();

        $item->Gestion = $x->format('Y');
        $item->Periodo = $request->Periodo;

        $item->UpdaterUserName = Auth::user()->email;
        $item->UpdaterFullUserName = Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }


    

    public function destroy(Request $request)
    {
        $item = Compania::where('id', $request->id)->first();
        $item->deleted_at = Carbon::now();
        $item->DeleterUserName = Auth::user()->Persona;
        $item->DeleterFullUserName = Auth::user()->Persona;
        $item->DeleterIP =  $request->ip();
        $item->save();
        $result = array(
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );

        return response()->json($result);
    }
}
