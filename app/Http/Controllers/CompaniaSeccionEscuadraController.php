<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Models\Compania;
use App\Models\Seccion;
use App\Models\Escuadra;
use App\Models\CompaniaSeccion;
use App\Models\CompaniaSeccionEscuadra;
use App\Http\Requests\CompaniaSeccionEscuadraRequest;

class CompaniaSeccionEscuadraController extends Controller
{
    //
    public function index()
    {
        

        $item = CompaniaSeccionEscuadra::from('CompaniaSeccionEscuadra as cse')
                ->leftJoin('CompaniaSeccion as cs', 'cse.CompaniaSeccion', '=', 'cs.id')
                ->leftJoin('Escuadra as e', 'cse.Escuadra', '=', 'e.id')
                ->whereNull('cse.deleted_at')
                ->select('cse.id','e.NombreEscuadra');



        /*return Datatables::of($item)
            ->addIndexColumn()
            ->addColumn('action', function ($p) {
                return '<a class="btn btn-info btn-xs btn-datatable-CompaniaSeccion" id="' . $p->id . '"><i class="fa fa-bars"></i> ' . 'Detalles' . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);*/
    }


    public function list(Request $request)
    {
        $item = new CompaniaSeccionEscuadra();
        $objeto = null;

        $objeto = $item->orderBy('id', 'asc')->where('CompaniaSeccion', $request->CompaniaSeccion)->whereNull('deleted_at')->with('escuadra')->get();

        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    //
    public function show(Request $request)

    {
        
        $item = CompaniaSeccionEscuadra::where('id', $request->id)->with('companiaSeccion','escuadra')->first();
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }




    public function store(CompaniaSeccionEscuadraRequest $request)
    {
        if ($request->id) {
            $item = CompaniaSeccionEscuadra::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new CompaniaSeccionEscuadra();
            $item->CreatorUserName = Auth::user()->email;
            $item->CreatorFullUserName = Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->CompaniaSeccion = $request->CompaniaSeccion;
        $item->Escuadra = $request->Escuadra;
       
        

        $item->UpdaterUserName = Auth::user()->email;
        $item->UpdaterFullUserName = Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }


    

    public function destroy(Request $request)
    {
        $item = CompaniaSeccionEscuadra::where('id', $request->id)->first();
        $item->deleted_at = Carbon::now();
        $item->DeleterUserName = Auth::user()->Persona;
        $item->DeleterFullUserName = Auth::user()->Persona;
        $item->DeleterIP =  $request->ip();
        $item->save();
        $result = array(
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );

        return response()->json($result);
    }
}
