<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Models\Compania;
use App\Models\Seccion;
use App\Models\CompaniaSeccion;
use App\Http\Requests\CompaniaSeccionRequest;


class CompaniaSeccionController extends Controller
{
    //
    public function index()
    {
        

        $item = CompaniaSeccion::from('CompaniaSeccion as cs')
                ->leftJoin('Compania as c', 'cs.Compania', '=', 'c.id')
                ->leftJoin('Seccion as s', 'cs.Seccion', '=', 's.id')
                ->whereNull('cs.deleted_at')
                ->select('cs.id','c.NombreCompania','s.NombreSeccion','s.Descripcion');



        return Datatables::of($item)
            ->addIndexColumn()
            ->addColumn('action', function ($p) {
                return '<a class="btn btn-info btn-xs btn-datatable-CompaniaSeccion" id="' . $p->id . '"><i class="fa fa-bars"></i> ' . 'Detalles' . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }




    public function list(Request $request)
    {
        $item = new CompaniaSeccion();
        $objeto = null;

        $objeto = $item->orderBy('id', 'asc')->whereNull('deleted_at')->get();

        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    //
    public function show(Request $request)

    {
        
        $item = CompaniaSeccion::where('id', $request->id)->with('compania','seccion')->first();
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }



    public function store(CompaniaSeccionRequest $request)
    {
        if ($request->id) {
            $item = CompaniaSeccion::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new CompaniaSeccion();
            $item->CreatorUserName = Auth::user()->email;
            $item->CreatorFullUserName = Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Compania = $request->Compania;
        $item->Seccion = $request->Seccion;
        

        $item->UpdaterUserName = Auth::user()->email;
        $item->UpdaterFullUserName = Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }


    

    public function destroy(Request $request)
    {
        $item = CompaniaSeccion::where('id', $request->id)->first();
        $item->deleted_at = Carbon::now();
        $item->DeleterUserName = Auth::user()->Persona;
        $item->DeleterFullUserName = Auth::user()->Persona;
        $item->DeleterIP =  $request->ip();
        $item->save();
        $result = array(
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );

        return response()->json($result);
    }
}
