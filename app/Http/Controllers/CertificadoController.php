<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use JasperPHP\Facades\JasperPHP;
use Yajra\Datatables\Datatables;
use App\Models\CompSecEscPersona;
use App\Models\CompaniaSeccionEscuadra;
use App\Models\CompaniaSeccion;
use App\Models\Asistencia;
use App\Models\EstadoAsistencia;
use App\Models\TipoAsistencia;
use App\Models\Escuadra;
use App\Models\Seccion;
use App\Models\Compania;


class CertificadoController extends Controller
{
    //
    public function index()
    {
        
        $compañias = CompaniaSeccionEscuadra::from('CompaniaSeccionEscuadra as cse')
                    ->Join('CompaniaSeccion as cs', 'cse.CompaniaSeccion', '=', 'cs.id')

                    ->Join('CompSecEscPersona as csep', 'csep.CompaniaSeccionEscuadra', '=', 'cse.id')

                    ->Join('Escuadra as e', 'cse.Escuadra', '=', 'e.id')

                    ->Join('Compania as c', 'cs.Compania', '=', 'c.id')
                    ->Join('Seccion as s', 'cs.Seccion', '=', 's.id')
                
                    ->whereNull('cse.deleted_at')
                    ->select('cse.id','e.NombreEscuadra','s.NombreSeccion', 'c.NombreCompania','c.Gestion','c.Periodo')
                    ->distinct();



        return Datatables::of($compañias)
            ->addIndexColumn()
            ->addColumn('action', function ($p) {
                return '<a class="btn btn-info btn-xs btn-datatable-Asistencia" id="' . $p->id . '"><i class="fa fa-bars"></i> ' . 'Detalles' . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);

    }


    //------------------------dataBaseConfig-------------------------------------------------    


    protected $basePathGenerated;
    protected $urlFile;

    public function getDatabaseConfig(){

        
        $jdbc_dir = 'C:\Users\WINDOWS\Desktop\Proyecto\starterv-2-backend\vendor\cossou\jasperphp\src\JasperStarter\jdbc';

        return[
            'driver'        => 'generic',
            'host'          => env('DB_HOST'),
            'port'          => env('DB_PORT'),
            'username'      => env('DB_USERNAME'),
            'password'      => env('DB_PASSWORD'),
            'database'      => env('DB_DATABASE'),
            'jdbc_driver'   => 'com.mysql.jdbc.Driver',
            'jdbc_url'      => 'jdbc:mysql://localhost/'.env('DB_DATABASE'),
            'jdbc_dir'      => $jdbc_dir

        ];

    }

    //-------------------metodo para la Certificacion--------------------------

    public function generar(Request $request)
    {

        //$item = Persona::where('id', $request->id)->with('unidadAcademica', 'rol')->first();

        //$foto = Persona::select('Foto')->where('id', $request->id)->first();
       
        //return ($foto);

        //$item = CompSecEscPersona::where('id', $request->id)->with('unidadAcademica', 'rol')->first();

        $extension = 'pdf';
        $nombre = 'certificacionCefor';
        $filenombre = $nombre . time();
        $output = base_path('public/tmp/'. $filenombre);
        $this->basePathGenerated = public_path('tmp/');
        $this->urlFile = config('app.url') . 'tmp/';
        JasperPHP::compile(storage_path('app/public').'/hojaCertificacion/hojaCertificacion.jrxml')->execute();



        $params = array('urlLogo' => public_path('images/emi_logo.png'));
        //                'FotoPersona' => public_path('storage/documents/documents/'.$foto->Foto) 
        

        $params['idPersona'] = $request->id;
        

        $reporteJasper = JasperPHP::process(
            
            storage_path('app/public/hojaCertificacion/hojaCertificacion.jasper'),
            $output,
            
            array($extension),
            $params,
            //array('IdPersona' => $request->id),
            $this->getDatabaseConfig(),
                
        );

        $reporteJasper->execute();
        
        //$file = $output .'.'.$extension;
        /*
        if(!file_exists($file)){
            abort(404);
        }
        */
        $pdf = array(
            'url' => $this->urlFile . $filenombre .'.pdf',
            'uri' => $this->basePathGenerated . $filenombre . '.pdf',
            'fileNombre' => $filenombre . '.pdf'
        );

        $data = array(
            'success' => true,
            'data' => $pdf,
            'msg' => 'Hoja de Certificacion Generado Correctamente'
        );
        //return response()->file($file)->deleteFileAfterSend();
        return response()->json($data);
        


    }
    
}
