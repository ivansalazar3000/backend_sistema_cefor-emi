<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Models\Escuadra;
use App\Models\Seccion;
use App\Models\Compania;
use App\Models\CompaniaSeccion;
use App\Models\CompaniaSeccionEscuadra;
use App\Models\CompSecEscPersona;
use App\Models\Persona;
use App\Models\EstadoPersona;
use App\Http\Requests\CompSecEscPersonaRequest;



class CompSecEscPersonaController extends Controller
{
    //
    public function index()
    {
        

        $item = CompSecEscPersona::from('CompSecEscPersona as csep')
                ->leftJoin('CompaniaSeccionEscuadra as cse', 'csep.CompaniaSeccionEscuadra', '=', 'cse.id')
                ->leftJoin('Persona as p', 'csep.Persona', '=', 'p.id')
                ->leftJoin('EstadoPersona as ep', 'csep.EstadoPersona', '=', 'ep.id')
                ->whereNull('csep.deleted_at')
                ->select('csep.id','p.Persona', 'ep.EstadoPersona');



        /*return Datatables::of($item)
            ->addIndexColumn()
            ->addColumn('action', function ($p) {
                return '<a class="btn btn-info btn-xs btn-datatable-CompaniaSeccion" id="' . $p->id . '"><i class="fa fa-bars"></i> ' . 'Detalles' . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);*/
    }


    public function list(Request $request)
    {
        $item = new CompSecEscPersona();
        $objeto = null;

        //$objeto = $item->orderBy('id', 'asc')->where('CompaniaSeccionEscuadra',$request->CompaniaSeccionEscuadra)->whereNull('deleted_at')->get();
        $objeto = $item->orderBy('id', 'asc')->where('CompaniaSeccionEscuadra', $request->CompaniaSeccionEscuadra)->whereNull('deleted_at')->with('persona','estadoPersona')->get();
        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    //
    public function showInfoCefor(Request $request)

    {
        $item = CompSecEscPersona::from('CompSecEscPersona as csep')
                ->leftJoin('CompaniaSeccionEscuadra as cse', 'csep.CompaniaSeccionEscuadra', '=', 'cse.id')
                ->leftJoin('CompaniaSeccion as cs', 'cse.CompaniaSeccion', '=', 'cs.id')
                ->leftJoin('Escuadra as e', 'cse.Escuadra','=', 'e.id')
                ->leftJoin('Compania as c', 'cs.Compania', '=', 'c.id')
                ->leftJoin('Seccion as s', 'cs.Seccion', '=', 's.id')
                ->leftJoin('Persona as p', 'csep.Persona', '=', 'p.id')
                ->leftJoin('EstadoPersona as ep', 'csep.EstadoPersona', '=', 'ep.id')
                ->whereNull('csep.deleted_at')
                ->select('csep.id',
                 'p.Persona',
                 'p.Foto',
                 'c.NombreCompania',
                 's.NombreSeccion',
                 'e.NombreEscuadra',
                 //'csep.EstadoPersona',
                 'csep.CantidadAsistencia',
                 'csep.CantidadFalta',
                 'csep.CantidadPermiso',
                 'csep.RevistaMilitar',
                 'ep.EstadoPersona'
                )->where('csep.Persona','=', $request->id)->first();
        
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }





    public function show(Request $request)

    {
        
        $item = CompSecEscPersona::where('id', $request->id)->with('companiaSeccionEscuadra','persona','estadoPersona')->first();

        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }



    public function showEstado(Request $request)

    {
        
        $item = CompSecEscPersona::where('id', $request->id)->with('companiaSeccionEscuadra','persona','estadoPersona')->first();
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function storeEstado(Request $request)
    {

        if ($request->id) {
            $item = CompSecEscPersona::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new CompSecEscPersona();
            $item->CreatorUserName = Auth::user()->email;
            $item->CreatorFullUserName = Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->CompaniaSeccionEscuadra = $request->CompaniaSeccionEscuadra;
        $item->Persona = $request->Persona;
        $item->EstadoPersona = $request->EstadoPersona;
        
        

        $item->UpdaterUserName = Auth::user()->email;
        $item->UpdaterFullUserName = Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }






    public function store(CompSecEscPersonaRequest $request)
    {

        if ($request->id) {
            $item = CompSecEscPersona::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new CompSecEscPersona();
            $item->CreatorUserName = Auth::user()->email;
            $item->CreatorFullUserName = Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->CompaniaSeccionEscuadra = $request->CompaniaSeccionEscuadra;
        $item->Persona = $request->Persona;
        $item->EstadoPersona = $request->EstadoPersona;
        $item->RevistaMilitar = $request->RevistaMilitar;
        

        $item->UpdaterUserName = Auth::user()->email;
        $item->UpdaterFullUserName = Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }


    

    public function destroy(Request $request)
    {
        $item = CompSecEscPersona::where('id', $request->id)->first();
        $item->deleted_at = Carbon::now();
        $item->DeleterUserName = Auth::user()->Persona;
        $item->DeleterFullUserName = Auth::user()->Persona;
        $item->DeleterIP =  $request->ip();
        $item->save();
        $result = array(
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );

        return response()->json($result);
    }
}
